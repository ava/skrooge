/***************************************************************************
 * SPDX-FileCopyrightText: 2022 S. MANKOWSKI stephane@mankowski.fr
 * SPDX-FileCopyrightText: 2022 G. DE BURE support@mankowski.fr
 * SPDX-License-Identifier: GPL-3.0-or-later
 ***************************************************************************/
#ifndef SKGTRACKERPLUGIN_H
#define SKGTRACKERPLUGIN_H
/** @file
 * A skrooge plugin to track transactions.
*
* @author Stephane MANKOWSKI
 */
#include "skginterfaceplugin.h"
#include "ui_skgtrackerpluginwidget_pref.h"


/**
 * A skrooge plugin to track transactions
 */
class SKGTrackerPlugin : public SKGInterfacePlugin
{
    Q_OBJECT
    Q_INTERFACES(SKGInterfacePlugin)

public:
    /**
     * Default Constructor
     */
    explicit SKGTrackerPlugin(QWidget* iWidget, QObject* iParent, const QVariantList& iArg);

    /**
     * Default Destructor
     */
    ~SKGTrackerPlugin() override;

    /**
     * Called to initialise the plugin
     * @param iDocument the main document
     * @return true if the plugin is compatible with the document
     */
    bool setupActions(SKGDocument* iDocument) override;

    /**
     * The preference skeleton of the plugin.
     * @return The preference skeleton of the plugin
     */
    KConfigSkeleton* getPreferenceSkeleton() override;

    /**
     * The page widget of the plugin.
     * @return The page widget of the plugin
     */
    SKGTabPage* getWidget() override;

    /**
     * The title of the plugin.
     * @return The title of the plugin
     */
    QString title() const override;

    /**
     * The icon of the plugin.
     * @return The icon of the plugin
     */
    QString icon() const override;

    /**
     * The toolTip of the plugin.
     * @return The toolTip of the plugin
     */
    QString toolTip() const override;

    /**
     * The tips list of the plugin.
     * @return The tips list of the plugin
     */
    QStringList tips() const override;

    /**
     * Must be implemented to set the position of the plugin.
     * @return integer value between 0 and 999 (default = 999)
     */
    int getOrder() const override;

    /**
     * Must be implemented to know if a plugin must be display in pages chooser.
     * @return true of false (default = false)
     */
    bool isInPagesChooser() const override;

    /**
     * The advice list of the plugin.
     * @return The advice list of the plugin
     */
    SKGAdviceList advice(const QStringList& iIgnoredAdvice) override;

private Q_SLOTS:

private:
    Q_DISABLE_COPY(SKGTrackerPlugin)


    SKGDocument* m_currentBankDocument;

    Ui::skgtrackerplugin_pref ui{};
};

#endif  // SKGTRACKERPLUGIN_H
