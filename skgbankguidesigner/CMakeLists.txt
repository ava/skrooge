#***************************************************************************
#* SPDX-FileCopyrightText: 2022 S. MANKOWSKI stephane@mankowski.fr
#* SPDX-FileCopyrightText: 2022 G. DE BURE support@mankowski.fr
#* SPDX-License-Identifier: GPL-3.0-or-later
#***************************************************************************
MESSAGE( STATUS "..:: CMAKE SKGBANKGUIDESIGNER ::..")

PROJECT(SKGBANKGUI)

LINK_DIRECTORIES (${LIBRARY_OUTPUT_PATH})

IF(SKG_WEBENGINE)
    MESSAGE( STATUS "     Mode WebEngine")
    ADD_DEFINITIONS(-DSKG_WEBENGINE=${SKG_WEBENGINE})
ENDIF(SKG_WEBENGINE)
IF(SKG_WEBKIT)
    MESSAGE( STATUS "     Mode Webkit")
    ADD_DEFINITIONS(-DSKG_WEBKIT=${SKG_WEBKIT})
ENDIF(SKG_WEBKIT)

SET(skgbankguidesigner_SRCS ${skgbankgui_SRCS}
    skgquerycreatordesignerplugin.cpp
    skgbkwidgetcollectiondesignerplugin.cpp
    skgpredicatcreatordesignerplugin.cpp
    skgunitcomboboxdesignerplugin.cpp
)

SET(LIBS Qt5::Designer skgbankgui)

IF(SKG_WEBENGINE)
    SET(LIBS ${LIBS} Qt5::WebEngineWidgets)
ENDIF(SKG_WEBENGINE)
IF(SKG_WEBKIT)
    SET(LIBS ${LIBS} Qt5::WebKitWidgets)
ENDIF(SKG_WEBKIT)

ADD_LIBRARY(skgbankguidesigner SHARED ${skgbankguidesigner_SRCS})
TARGET_LINK_LIBRARIES(skgbankguidesigner LINK_PUBLIC ${LIBS})
GENERATE_EXPORT_HEADER(skgbankguidesigner BASE_NAME skgbankguidesigner)

########### install files ###############
IF(WIN32)
    INSTALL(TARGETS skgbankguidesigner LIBRARY ARCHIVE DESTINATION ${KDE_INSTALL_PLUGINDIR}/designer )
ELSE(WIN32)
    INSTALL(TARGETS skgbankguidesigner LIBRARY DESTINATION ${KDE_INSTALL_PLUGINDIR}/designer )
ENDIF(WIN32)
